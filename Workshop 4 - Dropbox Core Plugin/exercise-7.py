from datetime import datetime
from ch.systemsx.cisd.openbis.generic.shared.basic.dto import DataSetKind 

def process(transaction):
  dataSet = transaction.createNewDataSet("UNKNOWN", DataSetKind.CONTAINER)
  transaction.getLogger().info("CREATE DATA SET %s" % dataSet.getDataSetCode())
  exp_id = "/DEFAULT/DEFAULT/EXP-%s" % datetime.today().strftime("%Y%m%d-%H%M%S")
  exp = transaction.createNewExperiment(exp_id, "DEFAULT_EXPERIMENT")
  exp.setPropertyValue("$NAME", transaction.getIncoming().getName())
  dataSet.setExperiment(exp)
  components = []
  with open(transaction.getIncoming().getAbsolutePath(), 'r') as fin:
    for line in fin.readlines():
      for term in  line.strip().split(" "):
        splitted_term = term.split(":")
        component = transaction.createNewDataSet()
        component.setExperiment(exp)
        with open(transaction.createNewFile(component, splitted_term[0]), 'w') as fout:
          fout.write("value = %s\n" % splitted_term[-1])
        components.append(component.getDataSetCode())
  dataSet.setContainedDataSetCodes(components)
