from datetime import datetime

def process(transaction):
  dataSet = transaction.createNewDataSet()
  transaction.getLogger().info("CREATE DATA SET %s" % dataSet.getDataSetCode())
  exp_id = "/DEFAULT/DEFAULT/EXP-%s" % datetime.today().strftime("%Y%m%d-%H%M%S")
  exp = transaction.createNewExperiment(exp_id, "DEFAULT_EXPERIMENT")
  exp.setPropertyValue("$NAME", transaction.getIncoming().getName())
  dataSet.setExperiment(exp)
  transaction.createNewDirectory(dataSet, "data")
  with open(transaction.getIncoming().getAbsolutePath(), 'r') as fin:
    for line in fin.readlines():
      for term in  line.strip().split(" "):
        splitted_term = term.split(":")
        with open(transaction.createNewFile(dataSet, "data", splitted_term[0]), 'w') as fout:
          fout.write("value = %s\n" % splitted_term[-1])
