from datetime import datetime
from ch.systemsx.cisd.openbis.generic.shared.basic.dto import DataSetKind
from ch.systemsx.cisd.openbis.dss.generic.shared import ServiceProvider
from ch.ethz.sis.openbis.generic.asapi.v3.dto.experiment.search import ExperimentSearchCriteria
from ch.ethz.sis.openbis.generic.asapi.v3.dto.experiment.fetchoptions import ExperimentFetchOptions
from ch.systemsx.cisd.common.mail import EMailAddress

def process(transaction):
  dataSet = transaction.createNewDataSet("UNKNOWN", DataSetKind.CONTAINER)
  transaction.getLogger().info("CREATE DATA SET %s" % dataSet.getDataSetCode())
  service = ServiceProvider.getV3ApplicationService()
  sessionToken = transaction.getOpenBisServiceSessionToken()
  searchCriteria = ExperimentSearchCriteria()
  searchCriteria.withProperty("$NAME").thatEquals(transaction.getIncoming().getName())
  fetchOptions = ExperimentFetchOptions()
  fetchOptions.sortBy().registrationDate().asc()
  experiments = service.searchExperiments(sessionToken, searchCriteria, fetchOptions).getObjects()
  if len(experiments) > 0:
    exp = transaction.getExperiment(experiments[0].getIdentifier().getIdentifier())
  else:
    exp_id = "/DEFAULT/DEFAULT/EXP-%s" % datetime.today().strftime("%Y%m%d-%H%M%S")
    exp = transaction.createNewExperiment(exp_id, "DEFAULT_EXPERIMENT")
    exp.setPropertyValue("$NAME", transaction.getIncoming().getName())
  dataSet.setExperiment(exp)
  components = []
  with open(transaction.getIncoming().getAbsolutePath(), 'r') as fin:
    for line in fin.readlines():
      for term in  line.strip().split(" "):
        splitted_term = term.split(":")
        component = transaction.createNewDataSet()
        component.setExperiment(exp)
        with open(transaction.createNewFile(component, splitted_term[0]), 'w') as fout:
          fout.write("value = %s\n" % splitted_term[-1])
        components.append(component.getDataSetCode())
  dataSet.setContainedDataSetCodes(components)
  transaction.getRegistrationContext().getPersistentMap().put("dataSetCode", dataSet.getDataSetCode())

def post_storage(context):
  addresses = []
  for adminEMail in context.getGlobalState().getAdministratorEmails():
    addresses.append(EMailAddress(adminEMail))
  mailClient = context.getGlobalState().getMailClient()
  message = "Data set %s has been successfully registered." % context.getPersistentMap().get("dataSetCode")
  mailClient.sendEmailMessage("Successful data set registration", message, None, None, addresses)
