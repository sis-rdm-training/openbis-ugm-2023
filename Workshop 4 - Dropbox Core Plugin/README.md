# Automating Data Imports: The Dropbox Core Plugin

## Exercise 1 - UGM VM login and openBIS start up
1. Start an SSH session with the command `ssh ugm<number>@<number>.ugm-training.openbis.ch` and authenticate with your password.
2. Run `su - openbis` to be logged in as the openbis user, who does not have the root privileges. 
3. Start openBIS (AS and DSS) with `./openbis/bin/allup.sh`. For stopping the servers you can do `./openbis/bin/alldown.sh`.
4. Connect to the instance using SSH, FTP or webDAV.

## Exercise 2 - openBIS Login and Experiment Creation
1. Open a browser and enter URL https://<number>.ugm-training.openbis.ch/openbis/webapp/eln-lims/.
2. Login with admin / changeit.
3. Navigate to `Lab Notebook > Others > Default Lab Notebook > Default Project` and create a Default Experiment with name `experiment1`. To do it, go to `New > Default Experiment`, enter the name and press Save.

## Exercise 3 - Dropbox Plugin Creation
1. Using the command line create the folders `openbis/servers/core-plugins/workshop/1/dss/drop-boxes/my-dropbox`:
```sh
mkdir -p openbis/servers/core-plugins/workshop/1/dss/drop-boxes/my-dropbox
```
2. Append the module `workshop` at the end of the property `enabled-modules` of the file `openbis/servers/core-plugins/core-plugins.properties`. Do not forget a comma before `workshop`.
3. Inside this folder, create the file `plugin.properties`
```sh
openbis/servers/core-plugins/workshop/1/dss/drop-boxes/my-dropbox/plugin.properties
```
with the following content:
```
incoming-dir = ${incoming-root-dir}/incoming-my-dropbox
top-level-data-set-handler = ch.systemsx.cisd.etlserver.registrator.api.v2.JythonTopLevelDataSetHandlerV2
incoming-data-completeness-condition = auto-detection
script-path = my-dropbox.py
storage-processor = ch.systemsx.cisd.etlserver.DefaultStorageProcessor
```
4. Create inside the same folder the file `my-dropbox.py` 
```sh
openbis/servers/core-plugins/workshop/1/dss/drop-boxes/my-dropbox/my-dropbox.py
``` 
with the following content:
```python
def process(transaction):
  dataSet = transaction.createNewDataSet()
  exp = transaction.getExperiment("/DEFAULT/DEFAULT/DEFAULT")
  dataSet.setExperiment(exp)
  transaction.moveFile(transaction.getIncoming().getAbsolutePath(), dataSet)
```

## Exercise 4 - DSS Restart and Data Set Registration
1. Restart DSS with `./openbis/bin/dssdown.sh && ./openbis/bin/dssup.sh`
2. Using `./openbis/bin/dsslog.sh`, in the DSS log, find the line with `incoming-my-dropbox` and verify that the path mentioned in this log event exists. Using the `/` key for search or just by paging using `u` and `d`.
3. Write a little text file into this incoming folder with the command 
```sh
echo Hello > store/incoming-my-dropbox/hello.txt
```
4. Watch the DSS log until event `Successfully committed transaction`. You can use the `./openbis/bin/dsslog.sh` command, however preferably from now on use the Dropbox Monitor https://<number>.ugm-training.openbis.ch/openbis/webapp/dropBoxMonitor. There is also the same monitor in ELN under `Utilities > Dropbox Monitor`.
5. In the ELN, navigate to `Lab Notebook > Others > Default > Default > DEFAULT`
6. Click on the child element (which should be the just registered data set).
7. Expand the data set file view (click on the little '+' button) and see the file `hello.txt`.
8. Click on `hello.txt`. A new browser tab opens with the content of the file.

## Exercise 5 - Data Set Registration with Marker File
1. Change property `incoming-data-completeness-condition` to `marker-file` in `plugin.properties`.
```sh
openbis/servers/core-plugins/workshop/1/dss/drop-boxes/my-dropbox/plugin.properties
```
2. Restart DSS with `./openbis/bin/dssdown.sh && ./openbis/bin/dssup.sh`
3. Create folder `store/incoming-my-dropbox/my-data` and a file in it.
```sh
mkdir -p store/incoming-my-dropbox/my-data
echo Another hello > store/incoming-my-dropbox/my-data/another-hello.txt
```
4. Create the empty file `store/incoming-my-dropbox/.MARKER_is_finished_my-data`:
```sh
touch store/incoming-my-dropbox/.MARKER_is_finished_my-data
```
5. Watch the DSS log registration finish.
6. In the ELN, navigate to `Lab Notebook > Others > Default > Default > DEFAULT`
7. Click on the 2nd child element under Data Sets (which should be the just registered data set).
8. Verify that the file has been registered.
9. Change the property `incoming-data-completeness-condition` back to `auto-detection` in `plugin.properties`:
```sh
openbis/servers/core-plugins/workshop/1/dss/drop-boxes/my-dropbox/plugin.properties
```
10. Restart DSS with `./openbis/bin/dssdown.sh && ./openbis/bin/dssup.sh`

## Exercise 6 - Dropbox Script: Logging, File Creation, Experiment Creation
1. Open `my-dropbox.py` for editing.
```sh
openbis/servers/core-plugins/workshop/1/dss/drop-boxes/my-dropbox/my-dropbox.py
```
2. Add the following line just after the data set creation statement (`dataSet = ...`):
```python
  transaction.getLogger().info("CREATE DATA SET %s" % dataSet.getDataSetCode())
```
3. Replace the `transaction.moveFile(...)` statement with the following code:
```python
  transaction.createNewDirectory(dataSet, "data")
  with open(transaction.getIncoming().getAbsolutePath(), 'r') as fin:
    for line in fin.readlines():
      for term in  line.strip().split(" "):
        splitted_term = term.split(":")
        with open(transaction.createNewFile(dataSet, "data", splitted_term[0]), 'w') as fout:
          fout.write("value = %s\n" % splitted_term[-1])
```
4. Add the following import at the top of the script:	
```python
from datetime import datetime
```
5. Replace the statement `exp = ...` with the following code: 
```python
  exp_id = "/DEFAULT/DEFAULT/EXP-%s" % datetime.today().strftime("%Y%m%d-%H%M%S")
  exp = transaction.createNewExperiment(exp_id, "DEFAULT_EXPERIMENT")
  exp.setPropertyValue("$NAME", transaction.getIncoming().getName())
```
6. Save the file and exit the editor.
7. Create a data file as follows:
```sh
echo "alpha:19.28 beta:35.42" > store/incoming-my-dropbox/data18372
```
8. Watch the DSS log. There should be an event containing `CREATE DATA SET`.
9. In the ELN, find the new experiment with name `data18372` and a data set with two files. Verify that the experiment code has the correct timestamp.

## Exercise 7 - Dropbox Script: Register Container Data Set with Components
1. Open `my-dropbox.py` for editing:
```sh
openbis/servers/core-plugins/workshop/1/dss/drop-boxes/my-dropbox/my-dropbox.py
```
2. Add the following lines in `my-dropbox.py` just after the first import statement:
```python
from ch.systemsx.cisd.openbis.generic.shared.basic.dto import DataSetKind 	
```
3. Replace the statement `dataset = ...` with the following code:
```python
  dataSet = transaction.createNewDataSet("UNKNOWN", DataSetKind.CONTAINER)
```
4. Replace the statement `transaction.createNewDirectory(...)` and the rest of the code with the following code:
```python
  components = []
  with open(transaction.getIncoming().getAbsolutePath(), 'r') as fin:
    for line in fin.readlines():
      for term in  line.strip().split(" "):
        splitted_term = term.split(":")
        component = transaction.createNewDataSet()
        component.setExperiment(exp)
        with open(transaction.createNewFile(component, splitted_term[0]), 'w') as fout:
          fout.write("value = %s\n" % splitted_term[-1])
        components.append(component.getDataSetCode())
  dataSet.setContainedDataSetCodes(components)
```
5. Save the file and exit the editor.
6. Create a data file as follows:
```sh
echo "pi:3.14159265358 e:2.71828" > store/incoming-my-dropbox/experiment1
```
7. Watch Dropbox Monitor until registration has been finished.
8. The new experiment named `experiment1` (in project `/DEFAULT/DEFAULT`) has 3 data sets. The first one is the container data sets which shows the combine file tree of the two other data sets.

## Exercise 8 - Dropbox Script: Using V3 API
1. Open `my-dropbox.py` for editing:
```sh
openbis/servers/core-plugins/workshop/1/dss/drop-boxes/my-dropbox/my-dropbox.py
```
2. Add the following lines in `my-dropbox.py` just after the other import statements:
```python
from ch.systemsx.cisd.openbis.dss.generic.shared import ServiceProvider
from ch.ethz.sis.openbis.generic.asapi.v3.dto.experiment.search import ExperimentSearchCriteria
from ch.ethz.sis.openbis.generic.asapi.v3.dto.experiment.fetchoptions import ExperimentFetchOptions
```
3. Before the statement `exp_id = ...`, add the following code:
```python
  service = ServiceProvider.getV3ApplicationService()
  sessionToken = transaction.getOpenBisServiceSessionToken()
  searchCriteria = ExperimentSearchCriteria()
  searchCriteria.withProperty("$NAME").thatEquals(transaction.getIncoming().getName())
  fetchOptions = ExperimentFetchOptions()
  fetchOptions.sortBy().registrationDate().asc()
  experiments = service.searchExperiments(sessionToken, searchCriteria, fetchOptions).getObjects()
  if len(experiments) > 0:
    exp = transaction.getExperiment(experiments[0].getIdentifier().getIdentifier())
  else:
```
4. Indent the 3 lines starting with the line `exp_id = ...` in order to be a part of the else clause.
5. Save the file and exit the editor.
6. Create a data file as follows:
```bash
echo "answer:42" > store/incoming-my-dropbox/experiment1
```
7. Watch Dropbox Monitor until registration has been finished.
8. Verify that the new data sets are registered for the experiment named `experiment1` which has been created in step 2.3 (i.e. in project `/DEFAULT_LAB_NOTEBOOK/DEFAULT_PROJECT`).

## Exercise 9 - Dropbox Script: Using a Hook to Send a Success Mail
1. Open `my-dropbox.py` for editing:
```sh
openbis/servers/core-plugins/workshop/1/dss/drop-boxes/my-dropbox/my-dropbox.py
```
2. Add the following lines after the other import statements:
```python
from ch.systemsx.cisd.common.mail import EMailAddress
```
3. Add at the end of the script the following code:
```python
  transaction.getRegistrationContext().getPersistentMap().put("dataSetCode", dataSet.getDataSetCode())

def post_storage(context):
  addresses = []
  for adminEMail in context.getGlobalState().getAdministratorEmails():
    addresses.append(EMailAddress(adminEMail))
  mailClient = context.getGlobalState().getMailClient()
  message = "Data set %s has been successfully registered." % context.getPersistentMap().get("dataSetCode")
  mailClient.sendEmailMessage("Successful data set registration", message, None, None, addresses)
```
4. Save the file and exit the editor.
5. Register another data set.
```sh
echo "answer:1" > store/incoming-my-dropbox/experiment1
```
6. Watch Dropbox Monitor until registration has been finished.
7. Verify that `store` folder contains a text file `email_...`
```sh
ls -al store/email_*
```
8. Verify that the file contains a text with the correct data set code:
```sh
ls -al store/email_* | xargs cat
```
