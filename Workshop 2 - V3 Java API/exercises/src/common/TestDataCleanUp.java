package common;

import java.util.List;

import org.apache.commons.lang3.exception.ExceptionUtils;

import ch.ethz.sis.openbis.generic.OpenBIS;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.deletion.id.IDeletionId;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.entitytype.id.EntityTypePermId;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.entitytype.id.IEntityTypeId;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.experiment.delete.ExperimentDeletionOptions;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.experiment.delete.ExperimentTypeDeletionOptions;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.experiment.id.ExperimentIdentifier;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.experiment.id.IExperimentId;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.project.delete.ProjectDeletionOptions;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.project.id.IProjectId;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.project.id.ProjectIdentifier;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.property.delete.PropertyTypeDeletionOptions;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.property.id.IPropertyTypeId;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.property.id.PropertyTypePermId;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.delete.SampleDeletionOptions;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.delete.SampleTypeDeletionOptions;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.id.ISampleId;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.id.SampleIdentifier;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.space.delete.SpaceDeletionOptions;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.space.id.ISpaceId;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.space.id.SpacePermId;

public class TestDataCleanUp
{
    public static void main(String[] args)
    {
        OpenBIS v3 = new OpenBIS(TestData.URL);
        v3.login(TestData.USER, TestData.PASSWORD);

        deleteSamples(v3);
        deleteExperiments(v3);
        deleteProjects(v3);
        deleteSpaces(v3);
        deleteExperimentTypes(v3);
        deleteSampleTypes(v3);
        deletePropertyTypes(v3);
    }

    private static void deletePropertyTypes(OpenBIS v3)
    {
        try
        {
            List<IPropertyTypeId> ids = List.of(new PropertyTypePermId("ELEVATION"), new PropertyTypePermId("POPULATION"));
            PropertyTypeDeletionOptions options = new PropertyTypeDeletionOptions();
            options.setReason("clean up");

            v3.deletePropertyTypes(ids, options);
        } catch (Exception e)
        {
            ExceptionUtils.printRootCauseStackTrace(e);
        }
    }

    private static void deleteExperimentTypes(OpenBIS v3)
    {
        try
        {
            List<IEntityTypeId> ids = List.of(new EntityTypePermId("LAND"));
            ExperimentTypeDeletionOptions options = new ExperimentTypeDeletionOptions();
            options.setReason("clean up");

            v3.deleteExperimentTypes(ids, options);
        } catch (Exception e)
        {
            ExceptionUtils.printRootCauseStackTrace(e);
        }
    }

    private static void deleteSampleTypes(OpenBIS v3)
    {
        try
        {
            List<IEntityTypeId> ids = List.of(new EntityTypePermId("TOWN"), new EntityTypePermId("SUMMIT"));
            SampleTypeDeletionOptions options = new SampleTypeDeletionOptions();
            options.setReason("clean up");

            v3.deleteSampleTypes(ids, options);
        } catch (Exception e)
        {
            ExceptionUtils.printRootCauseStackTrace(e);
        }
    }

    private static void deleteSpaces(OpenBIS v3)
    {
        try
        {
            List<ISpaceId> ids = List.of(new SpacePermId("WORKSHOP_V3_JAVA_API"));
            SpaceDeletionOptions options = new SpaceDeletionOptions();
            options.setReason("clean up");

            v3.deleteSpaces(ids, options);
        } catch (Exception e)
        {
            ExceptionUtils.printRootCauseStackTrace(e);
        }
    }

    private static void deleteProjects(OpenBIS v3)
    {
        try
        {
            List<IProjectId> ids = List.of(new ProjectIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND"));
            ProjectDeletionOptions options = new ProjectDeletionOptions();
            options.setReason("clean up");

            v3.deleteProjects(ids, options);
        } catch (Exception e)
        {
            ExceptionUtils.printRootCauseStackTrace(e);
        }
    }

    private static void deleteExperiments(OpenBIS v3)
    {
        try
        {
            List<IExperimentId> ids = List.of(new ExperimentIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/BERN"),
                    new ExperimentIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/OBWALDEN"),
                    new ExperimentIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/ST_GALLEN"),
                    new ExperimentIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/VALAIS"));
            ExperimentDeletionOptions options = new ExperimentDeletionOptions();
            options.setReason("clean up");

            IDeletionId iDeletionId = v3.deleteExperiments(ids, options);

            if (iDeletionId != null)
            {
                v3.confirmDeletions(List.of(iDeletionId));
            }
        } catch (Exception e)
        {
            ExceptionUtils.printRootCauseStackTrace(e);
        }
    }

    private static void deleteSamples(OpenBIS v3)
    {
        try
        {
            List<ISampleId> ids = List.of(new SampleIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/BERN"),
                    new SampleIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/JUNGFRAU"),
                    new SampleIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/EIGER"),
                    new SampleIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/ALPNACH"),
                    new SampleIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/PILATUS"),
                    new SampleIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/ST_GALLEN"),
                    new SampleIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/SANTIS"),
                    new SampleIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/ZERMATT"),
                    new SampleIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/MATTERHORN"),
                    new SampleIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/OBERROTHORN"));

            SampleDeletionOptions options = new SampleDeletionOptions();
            options.setReason("clean up");

            IDeletionId iDeletionId = v3.deleteSamples(ids, options);

            if (iDeletionId != null)
            {
                v3.confirmDeletions(List.of(iDeletionId));
            }
        } catch (Exception e)
        {
            ExceptionUtils.printRootCauseStackTrace(e);
        }
    }

}
