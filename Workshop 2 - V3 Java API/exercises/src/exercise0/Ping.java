package exercise0;

import ch.ethz.sis.openbis.generic.OpenBIS;
import common.TestData;

public class Ping
{

    public static void main(String[] args)
    {
        OpenBIS v3 = new OpenBIS(TestData.URL);
        String sessionToken = v3.login(TestData.USER, TestData.PASSWORD);

        System.out.println("It works! Your session token is: " + sessionToken);

        v3.logout();
    }
}
