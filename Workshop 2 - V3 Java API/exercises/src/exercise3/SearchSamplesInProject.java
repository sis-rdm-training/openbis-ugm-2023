package exercise3;

import ch.ethz.sis.openbis.generic.OpenBIS;
import common.TestData;

public class SearchSamplesInProject
{
    public static void main(String[] args)
    {
        // EXERCISE
        //
        // Find all samples that have:
        //
        //      project = /WORKSHOP_V3_JAVA_API/SWITZERLAND
        //
        // Hints:
        //
        //      method: v3.searchSamples
        //      classes:
        //          ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.search.SampleSearchCriteria
        //          ch.ethz.sis.openbis.generic.asapi.v3.dto.project.id.ProjectIdentifier
        //          ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.fetchoptions.SampleFetchOptions
        //

        OpenBIS v3 = new OpenBIS(TestData.URL);
        v3.login(TestData.USER, TestData.PASSWORD);

        // TODO

        v3.logout();
    }
}