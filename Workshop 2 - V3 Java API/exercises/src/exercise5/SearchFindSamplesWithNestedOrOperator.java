package exercise5;

import ch.ethz.sis.openbis.generic.OpenBIS;
import common.TestData;

public class SearchFindSamplesWithNestedOrOperator
{
    public static void main(String[] args)
    {
        // EXERCISE
        //
        // Find all samples that have:
        //
        //      project = /WORKSHOP_V3_JAVA_API/SWITZERLAND
        //      AND type = SUMMIT
        //      AND (ELEVATION property greater than 4000 OR experiment = /WORKSHOP_V3_JAVA_API/SWITZERLAND/ST_GALLEN)
        //
        // Hints:
        //
        //      method: v3.searchSamples
        //      classes:
        //          ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.search.SampleSearchCriteria
        //          ch.ethz.sis.openbis.generic.asapi.v3.dto.project.id.ProjectIdentifier
        //          ch.ethz.sis.openbis.generic.asapi.v3.dto.experiment.id.ExperimentIdentifier
        //          ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.fetchoptions.SampleFetchOptions
        //

        OpenBIS v3 = new OpenBIS(TestData.URL);
        v3.login(TestData.USER, TestData.PASSWORD);

        // TODO

        v3.logout();
    }
}