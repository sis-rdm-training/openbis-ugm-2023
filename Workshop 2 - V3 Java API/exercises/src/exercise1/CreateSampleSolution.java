package exercise1;

import java.util.List;

import ch.ethz.sis.openbis.generic.OpenBIS;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.entitytype.id.EntityTypePermId;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.experiment.id.ExperimentIdentifier;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.create.SampleCreation;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.space.id.SpacePermId;
import common.TestData;

public class CreateSampleSolution
{
    public static void main(String[] args)
    {
        // EXERCISE
        //
        // Create a new sample with:
        //
        //      type: SUMMIT
        //      space: /WORKSHOP_V3_JAVA_API
        //      experiment: /WORKSHOP_V3_JAVA_API/SWITZERLAND/VALAIS
        //      code: OBERROTHORN
        //      ELEVATION property: 3400

        OpenBIS v3 = new OpenBIS(TestData.URL);
        v3.login(TestData.USER, TestData.PASSWORD);

        SampleCreation sampleCreation = new SampleCreation();
        sampleCreation.setTypeId(new EntityTypePermId("SUMMIT"));
        sampleCreation.setSpaceId(new SpacePermId("/WORKSHOP_V3_JAVA_API"));
        sampleCreation.setExperimentId(new ExperimentIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/VALAIS"));
        sampleCreation.setCode("OBERROTHORN");
        sampleCreation.setProperty("ELEVATION", "3400");

        v3.createSamples(List.of(sampleCreation));

        v3.logout();
    }
}