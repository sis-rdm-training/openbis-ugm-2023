package exercise1;

import ch.ethz.sis.openbis.generic.OpenBIS;
import common.TestData;

public class CreateSample
{
    public static void main(String[] args)
    {
        // EXERCISE
        //
        // Create a new sample with:
        //
        //      type: SUMMIT
        //      space: /WORKSHOP_V3_JAVA_API
        //      experiment: /WORKSHOP_V3_JAVA_API/SWITZERLAND/VALAIS
        //      code: OBERROTHORN
        //      ELEVATION property: 3400
        //
        // Hints:
        //
        //      method: v3.createSamples
        //      classes:
        //          ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.create.SampleCreation
        //          ch.ethz.sis.openbis.generic.asapi.v3.dto.entitytype.id.EntityTypePermId
        //          ch.ethz.sis.openbis.generic.asapi.v3.dto.space.id.SpacePermId
        //          ch.ethz.sis.openbis.generic.asapi.v3.dto.experiment.id.ExperimentIdentifier
        //

        OpenBIS v3 = new OpenBIS(TestData.URL);
        v3.login(TestData.USER, TestData.PASSWORD);

        // TODO

        v3.logout();
    }
}