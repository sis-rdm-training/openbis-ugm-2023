package exercise4;

import ch.ethz.sis.openbis.generic.OpenBIS;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.common.search.SearchResult;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.project.id.ProjectIdentifier;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.Sample;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.fetchoptions.SampleFetchOptions;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.search.SampleSearchCriteria;
import common.TestData;

public class SearchSamplesWithAndOperatorSolution
{
    public static void main(String[] args)
    {
        // EXERCISE
        //
        // Find all samples that have:
        //
        //      project = /WORKSHOP_V3_JAVA_API/SWITZERLAND
        //      AND type = SUMMIT
        //      AND ELEVATION property greater than 4000

        OpenBIS v3 = new OpenBIS(TestData.URL);
        v3.login(TestData.USER, TestData.PASSWORD);

        SampleSearchCriteria criteria = new SampleSearchCriteria();
        criteria.withAndOperator();
        criteria.withProject().withId().thatEquals(new ProjectIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND"));
        criteria.withType().withCode().thatEquals("SUMMIT");
        criteria.withNumberProperty("ELEVATION").thatIsGreaterThan(4000);

        SearchResult<Sample> result = v3.searchSamples(criteria, new SampleFetchOptions());

        for (Sample sample : result.getObjects())
        {
            System.out.println(sample.getIdentifier());
        }

        v3.logout();
    }
}