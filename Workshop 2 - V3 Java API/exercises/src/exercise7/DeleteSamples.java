package exercise7;

import ch.ethz.sis.openbis.generic.OpenBIS;
import common.TestData;

public class DeleteSamples
{
    public static void main(String[] args)
    {
        // EXERCISE
        //
        // Permanently delete sample /WORKSHOP_V3_JAVA_API/SWITZERLAND/OBERROTHORN,
        // i.e. first move the sample to Trash and then Confirm the deletion.
        //
        // Hints:
        //
        //      method:
        //          v3.deleteSamples
        //          v3.confirmDeletions
        //      classes:
        //          ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.id.SampleIdentifier
        //          ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.delete.SampleDeletionOptions
        //

        OpenBIS v3 = new OpenBIS(TestData.URL);
        v3.login(TestData.USER, TestData.PASSWORD);

        // TODO

        v3.logout();
    }
}
