package exercise6;

import ch.ethz.sis.openbis.generic.OpenBIS;
import common.TestData;

public class GetSamples
{
    public static void main(String[] args)
    {
        // EXERCISE
        //
        // Get samples with the following identifiers:
        //
        //      /WORKSHOP_V3_JAVA_API/SWITZERLAND/SANTIS
        //      /WORKSHOP_V3_JAVA_API/SWITZERLAND/EIGER
        //      /WORKSHOP_V3_JAVA_API/SWITZERLAND/ZERMATT
        //
        // The samples should be fetched with their experiments (lands) and properties (elevations).
        // Print the fetched data to the console.
        //
        // Hints:
        //
        //      method: v3.getSamples
        //      classes:
        //          ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.id.SampleIdentifier
        //          ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.fetchoptions.SampleFetchOptions
        //

        OpenBIS v3 = new OpenBIS(TestData.URL);
        v3.login(TestData.USER, TestData.PASSWORD);

        // TODO

        v3.logout();
    }
}
