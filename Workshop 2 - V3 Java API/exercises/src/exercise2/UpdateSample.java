package exercise2;

import ch.ethz.sis.openbis.generic.OpenBIS;
import common.TestData;

public class UpdateSample
{
    public static void main(String[] args)
    {
        // EXERCISE
        //
        // Update sample /WORKSHOP_V3_JAVA_API/SWITZERLAND/OBERROTHORN,
        // set ELEVATION property to 3414.
        //
        // Hints:
        //
        //      method: v3.updateSamples
        //      classes:
        //          ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.update.SampleUpdate
        //          ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.id.SampleIdentifier
        //

        OpenBIS v3 = new OpenBIS(TestData.URL);
        v3.login(TestData.USER, TestData.PASSWORD);

        // TODO

        v3.logout();
    }
}