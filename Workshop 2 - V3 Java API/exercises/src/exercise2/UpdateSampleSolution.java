package exercise2;

import java.util.List;

import ch.ethz.sis.openbis.generic.OpenBIS;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.id.SampleIdentifier;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.update.SampleUpdate;
import common.TestData;

public class UpdateSampleSolution
{
    public static void main(String[] args)
    {
        // EXERCISE
        //
        // Update sample /WORKSHOP_V3_JAVA_API/SWITZERLAND/OBERROTHORN,
        // set ELEVATION property to 3414.

        OpenBIS v3 = new OpenBIS(TestData.URL);
        v3.login(TestData.USER, TestData.PASSWORD);

        SampleUpdate sampleUpdate = new SampleUpdate();
        sampleUpdate.setSampleId(new SampleIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/OBERROTHORN"));
        sampleUpdate.setProperty("ELEVATION", "3414");

        v3.updateSamples(List.of(sampleUpdate));

        v3.logout();
    }
}