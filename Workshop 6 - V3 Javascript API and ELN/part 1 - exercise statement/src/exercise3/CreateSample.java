package exercise3;

import ch.ethz.sis.openbis.generic.asapi.v3.IApplicationServerApi;
import common.Openbis;

public class CreateSample
{
    public static void main(String[] args)
    {
        // EXERCISE
        //
        // Create a new sample with:
        //
        //      type: SUMMIT
        //      space: /WORKSHOP_V3_JAVA_API
        //      experiment: /WORKSHOP_V3_JAVA_API/SWITZERLAND/VALAIS
        //      code: OBERROTHORN
        //      ELEVATION property: 3400

        IApplicationServerApi v3 = Openbis.createApplicationServerApi();
        String sessionToken = v3.login(Openbis.USER, Openbis.PASSWORD);

        // TODO

        v3.logout(sessionToken);
    }
}