package exercise9;

import ch.ethz.sis.openbis.generic.asapi.v3.IApplicationServerApi;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.common.search.SearchResult;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.experiment.id.ExperimentIdentifier;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.project.id.ProjectIdentifier;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.Sample;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.fetchoptions.SampleFetchOptions;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.search.SampleSearchCriteria;
import common.Openbis;

public class FindFindSamplesWithNestedOrOperatorSolution
{
    public static void main(String[] args)
    {
        // EXERCISE
        //
        // Find all samples that have:
        //
        //      project = /WORKSHOP_V3_JAVA_API/SWITZERLAND
        //      AND type = SUMMIT
        //      AND (ELEVATION property greater than 4000 OR experiment = /WORKSHOP_V3_JAVA_API/SWITZERLAND/ST_GALLEN)

        IApplicationServerApi v3 = Openbis.createApplicationServerApi();
        String sessionToken = v3.login(Openbis.USER, Openbis.PASSWORD);

        SampleSearchCriteria criteria = new SampleSearchCriteria();
        criteria.withAndOperator();
        criteria.withProject().withId().thatEquals(new ProjectIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND"));
        criteria.withType().withCode().thatEquals("SUMMIT");

        SampleSearchCriteria subCriteria = criteria.withSubcriteria();
        subCriteria.withOrOperator();
        subCriteria.withNumberProperty("ELEVATION").thatIsGreaterThan(4000);
        subCriteria.withExperiment().withId().thatEquals(new ExperimentIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/ST_GALLEN"));

        SearchResult<Sample> result = v3.searchSamples(sessionToken, criteria, new SampleFetchOptions());

        for (Sample sample : result.getObjects())
        {
            System.out.println(sample.getIdentifier());
        }

        v3.logout(sessionToken);
    }
}