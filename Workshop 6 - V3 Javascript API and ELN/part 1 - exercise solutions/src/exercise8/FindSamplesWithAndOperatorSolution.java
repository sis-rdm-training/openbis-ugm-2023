package exercise8;

import ch.ethz.sis.openbis.generic.asapi.v3.IApplicationServerApi;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.common.search.SearchResult;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.project.id.ProjectIdentifier;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.Sample;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.fetchoptions.SampleFetchOptions;
import ch.ethz.sis.openbis.generic.asapi.v3.dto.sample.search.SampleSearchCriteria;
import common.Openbis;

public class FindSamplesWithAndOperatorSolution
{
    public static void main(String[] args)
    {
        // EXERCISE
        //
        // Find all samples that have:
        //
        //      project = /WORKSHOP_V3_JAVA_API/SWITZERLAND
        //      AND type = SUMMIT
        //      AND ELEVATION property greater than 4000

        IApplicationServerApi v3 = Openbis.createApplicationServerApi();
        String sessionToken = v3.login(Openbis.USER, Openbis.PASSWORD);

        SampleSearchCriteria criteria = new SampleSearchCriteria();
        criteria.withAndOperator();
        criteria.withProject().withId().thatEquals(new ProjectIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND"));
        criteria.withType().withCode().thatEquals("SUMMIT");
        criteria.withNumberProperty("ELEVATION").thatIsGreaterThan(4000);

        SearchResult<Sample> result = v3.searchSamples(sessionToken, criteria, new SampleFetchOptions());

        for (Sample sample : result.getObjects())
        {
            System.out.println(sample.getIdentifier());
        }

        v3.logout(sessionToken);
    }
}