# Integrating with other systems and web openBIS V3 API (JavaScript)

## What is this Workshop about?
* Part 1 - How to use `openBIS V3 API from Javascript`
* Part 2 - How to extend the openBIS backend using `Custom Application Service Plugin` and using from `openBIS V3 API from Javascript`
* Part 3 - How to extend openBIS ELN-LIMS using `ELN-LIMS Plugin Interface`

## Why you need a Javascript API?
* The main goal is to build custom web User Interfaces.
* The Admin UI is build using only the openBIS Javascript V3 API.

## What is needed?
1. openBIS instance to create a web applications to use `openBIS V3 API from Javascript`.
2. openBIS instance to deploy a `Custom Application Service Plugin`.
3. openBIS instance to deploy and modify `ELN-LIMS Plugin`.
4. Development Environment: A text editor with Javascript syntax highlighting. Recomended: [Visual Studio Code](https://code.visualstudio.com)
5. File Manager: To copy files to the openBIS instance core plugins folder. Recomended: [Cyberduck](https://cyberduck.io)

## Part 1 - How to use `openBIS V3 API from Javascript`

### Javascript V3 API - Overview

Created to be a carbon copy of the native Java API. Consequences of that decision are:
* Has all same features!
* IApplicationServerApi
* Users and Groups
* Master data 
* Meta data
* IDataStoreServerApi
* Data files

It Shares the same [V3 API documentation](https://unlimited.ethz.ch/display/openBISDoc2010/openBIS+V3+API)!

You only need to learn the V3 API once!

### CORS
* Short Explanation: To be able to create an application from an external domain in needed special configuration
* Long Explanation: https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS

In openBIS:
1. Add at `~/openbis/servers/openBIS-server/jetty/etc/service.properties` the line `trusted-cross-origin-domains= *` .
2. Restart openBIS.

```bash
~/openbis/bin/alldown.sh
~/openbis/bin/allup.sh
```

### Main Difference with Java V3 API
* The require enclosure, encapsulates the objects that are going to be used on that context.
* This is a good practice to avoid namespace collisions with other objects with the same names.
* There are other solutions to this problem that we may adopt on the future.

```javascript
        var USER = "admin";
        var PASS = "changeit";

        function login() {
            require([ "openbis" ], function(openbis) {
                // get a reference to AS API
                var v3 = new openbis();
                // login to obtain a session token (the token it is automatically stored in openbis object and will be used for all subsequent API calls)
                document.write("1.login");
                document.write("<br>");
                v3.login(USER, PASS).done(function(sessionToken) {
                    document.write("2.login - sessionToken: " + sessionToken);
                    document.write("<br>");
                    //TO-DO
                    // logout to release the resources related with the session
                    v3.logout();
                    document.write("3.logout");
                });
            });
        }

        login();
```

### Exercise 1 - Deploy a webapp core-plugin

Docker Host SFTP Port: 2222

```bash
URL: sftp://X.ugm-training.openbis.ch/~/openbis
Server: X.ugm-training.openbis.ch
Username: ugmX
Password: XXX
```

1. Deploy the provided `workshop-js-v3-api-plugin` on an openBIS instance `~/openbis/servers/core-plugins` folder.

2. Add such plugin to the `~/openbis/servers/core-plugins/core-plugins.properties` file.

```properties
enabled-modules = monitoring-support, dropbox-monitor, dataset-uploader, dataset-file-search, xls-import, openbis-sync, eln-lims, eln-lims-life-sciences, admin, search-store, workshop-js-v3-api-plugin
```

3. Reboot your openBIS instance to make it available.

Docker Container SSH Port: 22

```bash
ssh ugmX@X.ugm-training.openbis.ch
su openbis
/home/openbis/openbis/bin/alldown.sh
/home/openbis/openbis/bin/allup.sh
```

### Exercise 2 - Login/Logout

1. Access `https://openbis-instance.ethz.ch/openbis/webapp/workshop-js-v3-api/login.html` and ensure it can login/logout correctly using the V3 API as shown below.

```javascript
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login/Logout</title>
	<script type="text/javascript" src="../../resources/api/v3/config.bundle.js"></script>
	<script type="text/javascript" src="../../resources/api/v3/require.js"></script>
    <script>
        var USER = "admin";
        var PASS = "changeit";

        function login() {
            require([ "openbis" ], function(openbis) {
                // get a reference to AS API
                var v3 = new openbis();
                // login to obtain a session token (the token it is automatically stored in openbis object and will be used for all subsequent API calls)
                document.write("1.login");
                document.write("<br>");
                v3.login(USER, PASS).done(function(sessionToken) {
                    document.write("2.login - sessionToken: " + sessionToken);
                    document.write("<br>");

                    //TO-DO

                    // logout to release the resources related with the session
                    v3.logout();
                    document.write("3.logout");
                });
            });
        }

        login();
    </script>
</head>
<body>

</body>
</html>
```

The browser should show the following output:

```
1.login
2.login - sessionToken: admin-230816100108453xA6CC31332F34C1CC1488D081A06E34EC
3.logout
```

### Exercise 3 - Create Sample

1. Create a new page `create-sample.html` for the next exercise and look up the [sample creation example](https://unlimited.ethz.ch/display/openBISDoc2010/openBIS+V3+API#openBISV3API-Creatingentities) on the documentation:

```javascript
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Create Sample</title>
	<script type="text/javascript" src="../../resources/api/v3/config.bundle.js"></script>
	<script type="text/javascript" src="../../resources/api/v3/require.js"></script>
    <script>
        // EXERCISE
        //
        // Create a new sample with:
        //
        //      type: SUMMIT
        //      space: /WORKSHOP_V3_JAVA_API
        //      experiment: /WORKSHOP_V3_JAVA_API/SWITZERLAND/VALAIS
        //      code: OBERROTHORN
        //      ELEVATION property: 3400

        var USER = "admin";
        var PASS = "changeit";

        function login() {
            require([ "openbis" ], function(openbis) {
                // get a reference to AS API
                var v3 = new openbis();
                // login to obtain a session token (the token it is automatically stored in openbis object and will be used for all subsequent API calls)
                v3.login(USER, PASS).done(function(sessionToken) {
                    document.write("sessionToken: " + sessionToken);

                    //TO-DO

                    // logout to release the resources related with the session
                    v3.logout();
                });
            });
        }

        login();
    </script>
</head>
<body>

</body>
</html>
```

2. Replacing the previous `login` method for the next `create` method should give the intended result.

```javascript
        function create() {
            require([   "openbis",
                        "as/dto/sample/create/SampleCreation",
                        "as/dto/entitytype/id/EntityTypePermId",
                        "as/dto/space/id/SpacePermId",
                        "as/dto/experiment/id/ExperimentIdentifier"
                        ], // Classes used by the result can't fail to load anymore
                        function(openbis, SampleCreation, EntityTypePermId, SpacePermId, ExperimentIdentifier) {
                // get a reference to AS API
                var v3 = new openbis();
                // login to obtain a session token (the token it is automatically stored in openbis object and will be used for all subsequent API calls)
                v3.login(USER, PASS).done(function(sessionToken) {
                    document.write("sessionToken: " + sessionToken + "<br>");
                    var sampleCreation = new SampleCreation();
                    sampleCreation.setTypeId(new EntityTypePermId("SUMMIT"));
                    sampleCreation.setSpaceId(new SpacePermId("/WORKSHOP_V3_JAVA_API"));
                    sampleCreation.setExperimentId(new ExperimentIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/VALAIS"));
                    sampleCreation.setCode("OBERROTHORN-BUNDLE");
                    sampleCreation.setProperty("ELEVATION", "3400");
                    document.write("before call"+ "<br>");
                    v3.createSamples([sampleCreation]).done(function(result) {
                        document.write("after call done: " + JSON.stringify(result));
                        // logout to release the resources related with the session
                        v3.logout();
                    }).fail(function(error) {
                        document.write("after call fail: " + JSON.stringify(error));
                    });
                });
            });
        }

        create();
```

### Exercise 4 - Update Sample

1. Create a new page `update-sample.html` for the next exercise and look up the [sample update example](https://unlimited.ethz.ch/display/openBISDoc2010/openBIS+V3+API#openBISV3API-Updatingentities) on the documentation:

```javascript
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Update Sample</title>
	<script type="text/javascript" src="../../resources/api/v3/config.bundle.js"></script>
	<script type="text/javascript" src="../../resources/api/v3/require.js"></script>
    <script>
        // EXERCISE
        //
        // Update sample /WORKSHOP_V3_JAVA_API/SWITZERLAND/OBERROTHORN,
        // set ELEVATION property to 3414.

        var USER = "admin";
        var PASS = "changeit";

        function login() {
            require([ "openbis" ], function(openbis) {
                // get a reference to AS API
                var v3 = new openbis();
                // login to obtain a session token (the token it is automatically stored in openbis object and will be used for all subsequent API calls)
                v3.login(USER, PASS).done(function(sessionToken) {
                    document.write("sessionToken: " + sessionToken);

                    //TO-DO

                    // logout to release the resources related with the session
                    v3.logout();
                });
            });
        }

        login();
    </script>
</head>
<body>

</body>
</html>
```

2. Replacing the previous `login` method for the next `update` method should give the intended result.

```javascript
        function update() {
            require([   "openbis",
                        "as/dto/sample/update/SampleUpdate",
                        "as/dto/sample/id/SampleIdentifier"],
                        function(openbis, SampleUpdate, SampleIdentifier) {
                // get a reference to AS API
                var v3 = new openbis();
                // login to obtain a session token (the token it is automatically stored in openbis object and will be used for all subsequent API calls)
                v3.login(USER, PASS).done(function(sessionToken) {
                    document.write("sessionToken: " + sessionToken + "<br>");
                    var sampleUpdate = new SampleUpdate();
                    sampleUpdate.setSampleId(new SampleIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/OBERROTHORN"));
                    sampleUpdate.setProperty("ELEVATION", "3414");
                    document.write("before call"+ "<br>");
                    v3.updateSamples([sampleUpdate]).done(function() {
                        document.write("after call done: ");
                        // logout to release the resources related with the session
                        v3.logout();
                    }).fail(function(error) {
                        document.write("after call fail: " + JSON.stringify(error));
                    });
                });
            });
        }

        update();
```

### Exercise 5 - Search Samples by basic query

1. Create a new page `search-samples-by-basic-query.html` for the next exercise and look up the [sample search examples](https://unlimited.ethz.ch/display/openBISDoc2010/openBIS+V3+API#openBISV3API-Searchingentities) on the documentation:

```javascript
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Search Samples by Basic Query</title>
	<script type="text/javascript" src="../../resources/api/v3/config.bundle.js"></script>
	<script type="text/javascript" src="../../resources/api/v3/require.js"></script>
    <script>
        // EXERCISE
        //
        // Find all samples that have:
        //
        //      project = /WORKSHOP_V3_JAVA_API/SWITZERLAND

        var USER = "admin";
        var PASS = "changeit";

        function login() {
            require([ "openbis" ], function(openbis) {
                // get a reference to AS API
                var v3 = new openbis();
                // login to obtain a session token (the token it is automatically stored in openbis object and will be used for all subsequent API calls)
                v3.login(USER, PASS).done(function(sessionToken) {
                    document.write("sessionToken: " + sessionToken);

                    //TO-DO

                    // logout to release the resources related with the session
                    v3.logout();
                });
            });
        }

        login();
    </script>
</head>
<body>

</body>
</html>
```

2. Replacing the previous `login` method for the next `search` method should give the intended result.

```javascript
        function search() {
            require([   "openbis",
                        "as/dto/sample/search/SampleSearchCriteria",
                        "as/dto/project/id/ProjectIdentifier",
                        "as/dto/sample/fetchoptions/SampleFetchOptions",
                        "as/dto/sample/Sample","as/dto/sample/id/SamplePermId","as/dto/sample/id/SampleIdentifier"], // Used by the result
                        function(openbis, SampleSearchCriteria, ProjectIdentifier, SampleFetchOptions) {
                // get a reference to AS API
                var v3 = new openbis(AS_URL);
                // login to obtain a session token (the token it is automatically stored in openbis object and will be used for all subsequent API calls)
                v3.login(USER, PASS).done(function(sessionToken) {
                    document.write("sessionToken: " + sessionToken + "<br>");
                    var criteria = new SampleSearchCriteria();
                    criteria.withProject().withId().thatEquals(new ProjectIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND"));
                    document.write("before call"+ "<br>");
                    v3.searchSamples(criteria, new SampleFetchOptions()).done(function(results) {
                        document.write("after call done: " + JSON.stringify(results));
                        // logout to release the resources related with the session
                        v3.logout();
                    }).fail(function(error) {
                        document.write("after call fail: " + JSON.stringify(error));
                    });
                });
            });
        }

        search();
```

### Exercise 6 - Get Samples

1. Create a new page `get-samples.html` for the next exercise and look up the [get samples examples](https://unlimited.ethz.ch/display/openBISDoc2010/openBIS+V3+API#openBISV3API-Gettingentities) on the documentation:

```javascript
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Get Samples</title>
	<script type="text/javascript" src="../../resources/api/v3/config.bundle.js"></script>
	<script type="text/javascript" src="../../resources/api/v3/require.js"></script>
    <script>
        // EXERCISE
        //
        // Get samples with the following identifiers:
        //
        //      /WORKSHOP_V3_JAVA_API/SWITZERLAND/SANTIS
        //      /WORKSHOP_V3_JAVA_API/SWITZERLAND/EIGER
        //      /WORKSHOP_V3_JAVA_API/SWITZERLAND/ZERMATT
        //
        // The samples should be fetched with their experiments (lands) and properties (elevations).
        // Print the fetched data to the console.

        var USER = "admin";
        var PASS = "changeit";

        function login() {
            require([ "openbis" ], function(openbis) {
                // get a reference to AS API
                var v3 = new openbis();
                // login to obtain a session token (the token it is automatically stored in openbis object and will be used for all subsequent API calls)
                v3.login(USER, PASS).done(function(sessionToken) {
                    document.write("sessionToken: " + sessionToken);

                    //TO-DO

                    // logout to release the resources related with the session
                    v3.logout();
                });
            });
        }

        login();
    </script>
</head>
<body>

</body>
</html>
```

2. Replacing the previous `login` method for the next `get` method should give the intended result.

```javascript
        function get() {
            require([   "openbis",
                        "as/dto/sample/id/SampleIdentifier",
                        "as/dto/sample/fetchoptions/SampleFetchOptions",
                        "as/dto/sample/Sample" ,"as/dto/sample/id/SamplePermId", "as/dto/experiment/Experiment", "as/dto/experiment/id/ExperimentPermId", "as/dto/experiment/id/ExperimentIdentifier"], // Used by the result
                        function(openbis, SampleIdentifier, SampleFetchOptions) {
                // get a reference to AS API
                var v3 = new openbis();
                // login to obtain a session token (the token it is automatically stored in openbis object and will be used for all subsequent API calls)
                v3.login(USER, PASS).done(function(sessionToken) {
                    document.write("sessionToken: " + sessionToken + "<br>");

                    var fo = new SampleFetchOptions();
                    fo.withExperiment();
                    fo.withProperties();

                    document.write("before call"+ "<br>");
                    v3.getSamples([new SampleIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/SANTIS"),
                                    new SampleIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/EIGER"),
                                    new SampleIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/ZERMATT")], fo).done(function(result) {
                        document.write("after call done: " + JSON.stringify(result));
                        // logout to release the resources related with the session
                        v3.logout();
                    }).fail(function(error) {
                        document.write("after call fail: " + JSON.stringify(error));
                    });
                });
            });
        }

        get();
```

### Exercise 7 - Delete Samples

1. Create a new page `delete-samples.html` for the next exercise and look up the [Delete samples examples](https://unlimited.ethz.ch/display/openBISDoc2010/openBIS+V3+API#openBISV3API-Deletingentities) on the documentation:

```javascript
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Delete Samples</title>
	<script type="text/javascript" src="../../resources/api/v3/config.bundle.js"></script>
	<script type="text/javascript" src="../../resources/api/v3/require.js"></script>
    <script>
        // EXERCISE
        //
        // Permanently delete sample /WORKSHOP_V3_JAVA_API/SWITZERLAND/OBERROTHORN,
        // i.e. first move the sample to Trash and then Confirm the deletion.

        var USER = "admin";
        var PASS = "changeit";

        function login() {
            require([ "openbis" ], function(openbis) {
                // get a reference to AS API
                var v3 = new openbis();
                // login to obtain a session token (the token it is automatically stored in openbis object and will be used for all subsequent API calls)
                v3.login(USER, PASS).done(function(sessionToken) {
                    document.write("sessionToken: " + sessionToken);

                    //TO-DO

                    // logout to release the resources related with the session
                    v3.logout();
                });
            });
        }

        login();
    </script>
</head>
<body>

</body>
</html>
```

2. Replacing the previous `login` method for the next `deletes` method should give the intended result.

```javascript
        function deletes() {
            require([   "openbis",
                        "as/dto/sample/id/SampleIdentifier",
                        "as/dto/sample/delete/SampleDeletionOptions",
                        "as/dto/deletion/id/DeletionTechId"], // Used by the result
                        function(openbis, SampleIdentifier, SampleDeletionOptions) {
                // get a reference to AS API
                var v3 = new openbis();
                // login to obtain a session token (the token it is automatically stored in openbis object and will be used for all subsequent API calls)
                v3.login(USER, PASS).done(function(sessionToken) {
                    document.write("sessionToken: " + sessionToken + "<br>");

                        var deletionOptions = new SampleDeletionOptions();
                        deletionOptions.setReason("workshop exercise");
                        document.write("before call 1"+ "<br>");
                        v3.deleteSamples([new SampleIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/OBERROTHORN-5")], deletionOptions).done(function(deletionId) {
                        document.write("after call 1 done: " + JSON.stringify(deletionId) + "<br>");
                        document.write("before call 2"+ "<br>");
                        v3.confirmDeletions([deletionId]).done(function() {
                            document.write("after call 2 done: ");
                            // logout to release the resources related with the session
                            v3.logout();
                        }).fail(function(error) {
                        document.write("after call fail 2: " + JSON.stringify(error));
                        });
                    }).fail(function(error) {
                        document.write("after call fail 1: " + JSON.stringify(error));
                    });
                });
            });
        }

        deletes();
```

### Exercise 8 - Search Samples by Simple Query

1. Create a new page `search-samples-by-simple-query.html` for the next exercise and look up the [sample search examples](https://unlimited.ethz.ch/display/openBISDoc2010/openBIS+V3+API#openBISV3API-Searchingentities) on the documentation:

```javascript
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Search Samples by Simple Query</title>
	<script type="text/javascript" src="../../resources/api/v3/config.bundle.js"></script>
	<script type="text/javascript" src="../../resources/api/v3/require.js"></script>
    <script>
        // EXERCISE
        //
        // Find all samples that have:
        //
        //      project = /WORKSHOP_V3_JAVA_API/SWITZERLAND
        //      AND type = SUMMIT
        //      AND ELEVATION property greater than 4000

        var USER = "admin";
        var PASS = "changeit";

        function login() {
            require([ "openbis" ], function(openbis) {
                // get a reference to AS API
                var v3 = new openbis();
                // login to obtain a session token (the token it is automatically stored in openbis object and will be used for all subsequent API calls)
                v3.login(USER, PASS).done(function(sessionToken) {
                    document.write("sessionToken: " + sessionToken);

                    //TO-DO

                    // logout to release the resources related with the session
                    v3.logout();
                });
            });
        }

        login();
    </script>
</head>
<body>

</body>
</html>
```

2. Replacing the previous `login` method for the next `search` method should give the intended result.

```javascript
        function search() {
            require([   "openbis",
                        "as/dto/sample/search/SampleSearchCriteria",
                        "as/dto/project/id/ProjectIdentifier",
                        "as/dto/sample/fetchoptions/SampleFetchOptions",
                        "as/dto/sample/Sample","as/dto/sample/id/SamplePermId","as/dto/sample/id/SampleIdentifier"], // Used by the result
                        function(openbis, SampleSearchCriteria, ProjectIdentifier, SampleFetchOptions) {
                // get a reference to AS API
                var v3 = new openbis(AS_URL);
                // login to obtain a session token (the token it is automatically stored in openbis object and will be used for all subsequent API calls)
                v3.login(USER, PASS).done(function(sessionToken) {
                    document.write("sessionToken: " + sessionToken + "<br>");
                    var criteria = new SampleSearchCriteria();
                    criteria.withAndOperator();
                    criteria.withProject().withId().thatEquals(new ProjectIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND"));
                    criteria.withType().withCode().thatEquals("SUMMIT");
                    criteria.withNumberProperty("ELEVATION").thatIsGreaterThan(4000);
                    document.write("before call"+ "<br>");
                    v3.searchSamples(criteria, new SampleFetchOptions()).done(function(results) {
                        document.write("after call done: " + JSON.stringify(results));
                        // logout to release the resources related with the session
                        v3.logout();
                    }).fail(function(error) {
                        document.write("after call fail: " + JSON.stringify(error));
                    });
                });
            });
        }

        search();
```

### Exercise 9 - Search Samples by Complex Query

1. Create a new page `search-samples-by-complex-query.html` for the next exercise and look up the [sample search examples](https://unlimited.ethz.ch/display/openBISDoc2010/openBIS+V3+API#openBISV3API-Searchingentities) on the documentation:

```javascript
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
	<script type="text/javascript" src="../../resources/api/v3/config.bundle.js"></script>
	<script type="text/javascript" src="../../resources/api/v3/require.js"></script>
    <script>
        // EXERCISE
        //
        // Find all samples that have:
        //
        //      project = /WORKSHOP_V3_JAVA_API/SWITZERLAND
        //      AND type = SUMMIT
        //      AND (ELEVATION property greater than 4000 OR experiment = /WORKSHOP_V3_JAVA_API/SWITZERLAND/ST_GALLEN)

        var USER = "admin";
        var PASS = "changeit";

        function login() {
            require([ "openbis" ], function(openbis) {
                // get a reference to AS API
                var v3 = new openbis();
                // login to obtain a session token (the token it is automatically stored in openbis object and will be used for all subsequent API calls)
                v3.login(USER, PASS).done(function(sessionToken) {
                    document.write("sessionToken: " + sessionToken);

                    //TO-DO

                    // logout to release the resources related with the session
                    v3.logout();
                });
            });
        }

        login();
    </script>
</head>
<body>

</body>
</html>
```

2. Replacing the previous `login` method for the next `search` method should give the intended result.

```javascript
        function search() {
            require([   "openbis",
                        "as/dto/sample/search/SampleSearchCriteria",
                        "as/dto/project/id/ProjectIdentifier",
                        "as/dto/experiment/id/ExperimentIdentifier",
                        "as/dto/sample/fetchoptions/SampleFetchOptions",
                        "as/dto/experiment/search/ExperimentSearchCriteria", // Used by the nested criteria
                        "as/dto/sample/Sample","as/dto/sample/id/SamplePermId","as/dto/sample/id/SampleIdentifier"], // Used by the result
                        function(openbis, SampleSearchCriteria, ProjectIdentifier, ExperimentIdentifier, SampleFetchOptions) {
                // get a reference to AS API
                var v3 = new openbis(AS_URL);
                // login to obtain a session token (the token it is automatically stored in openbis object and will be used for all subsequent API calls)
                v3.login(USER, PASS).done(function(sessionToken) {
                    document.write("sessionToken: " + sessionToken + "<br>");
                    var criteria = new SampleSearchCriteria();
                    criteria.withAndOperator();
                    criteria.withProject().withId().thatEquals(new ProjectIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND"));
                    criteria.withType().withCode().thatEquals("SUMMIT");

                    var subCriteria = criteria.withSubcriteria();
                    subCriteria.withOrOperator();
                    subCriteria.withNumberProperty("ELEVATION").thatIsGreaterThan(4000);
                    subCriteria.withExperiment().withId().thatEquals(new ExperimentIdentifier("/WORKSHOP_V3_JAVA_API/SWITZERLAND/ST_GALLEN"));
                    document.write("before call"+ "<br>");
                    v3.searchSamples(criteria, new SampleFetchOptions()).done(function(results) {
                        document.write("after call done: " + JSON.stringify(results));
                        // logout to release the resources related with the session
                        v3.logout();
                    }).fail(function(error) {
                        document.write("after call fail: " + JSON.stringify(error));
                    });
                });
            });
        }

        search();
```

## Part 2 - How to extend the openBIS backend using `Custom Application Service Plugin` and using from `openBIS V3 API from Javascript`

### Why extending the openBIS backend is needed?
* CRUD (Create, Read, Update, Delete) operations don’t map well to business processes.
* Business processes often do several CRUD operations in one transaction.
Example: Multiple create operations + sending an email

### If is only necessary several CRUD operations Batch Operations could be used but often falls short:
* Business processes often enforce restrictions that are non enforceable on the client.
* Business processes often use as the input of the next operation the output of the last.

**The solution to this is to make your own API using [Custom Application Services](https://unlimited.ethz.ch/display/openBISDoc2010/Custom+Application+Server+Services)**
**With this goal Custom Application Server Services core plugins where created**

### Documentation

[Custom Application Services](https://unlimited.ethz.ch/display/openBISDoc2010/Custom+Application+Server+Services)

### Custom Application Server Service - Minimal Example

Folder Structure:

![Folder Structure](docs/custom-as-service-1of2-folders.png)

plugin.properties:

```
class = ch.ethz.sis.openbis.generic.server.asapi.v3.helper.service.JythonBasedCustomASServiceExecutor
script-path = script.py
```

script.py:

```python
def process(context, parameters):
	method = parameters.get("method")
	result = None
	try:
		if method == "echo":
			message = parameters.get("message")
			result = {
				"status" : "OK",
				"result" : message
			}
	except Exception, value:
		result = str(value)
	return result
```

### Exercise 10 - Deploy Custom AS Service

1. Deploy the provided `workshop-as-service-plugin` on an openBIS instance `~/openbis/servers/core-plugins` folder.

2. Add such plugin to the `~/openbis/servers/core-plugins/core-plugins.properties` file.

```properties
enabled-modules = monitoring-support, dropbox-monitor, dataset-uploader, dataset-file-search, xls-import, openbis-sync, eln-lims, eln-lims-life-sciences, admin, search-store, workshop-js-v3-api-plugin, workshop-as-service-plugin
```

3. Reboot your openBIS instance to make it available.

```bash
~/openbis/bin/alldown.sh
~/openbis/bin/allup.sh
```

### Exercise 11 - Call the Custom AS Service

1. Create a new page `query-custom-as-service.html` for the next exercise and look up the [custom as services](https://unlimited.ethz.ch/display/openBISDoc2010/openBIS+V3+API#openBISV3API-CustomASServices) on the documentation:

```javascript
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
	<script type="text/javascript" src="../../resources/api/v3/config.bundle.js"></script>
	<script type="text/javascript" src="../../resources/api/v3/require.js"></script>
    <script>

        var USER = "admin";
        var PASS = "changeit";

        function login() {
            require([ "openbis" ], function(openbis) {
                // get a reference to AS API
                var v3 = new openbis();
                // login to obtain a session token (the token it is automatically stored in openbis object and will be used for all subsequent API calls)
                v3.login(USER, PASS).done(function(sessionToken) {
                    document.write("sessionToken: " + sessionToken);

                    //TO-DO

                    // logout to release the resources related with the session
                    v3.logout();
                });
            });
        }

        login();
    </script>
</head>
<body>

</body>
</html>
```

2. Replacing the previous `login` method for the next `executeCustomASService` method should give the intended result.

```javascript
        function executeCustomASService() {
            require([ "openbis", "as/dto/service/id/CustomASServiceCode", "as/dto/service/CustomASServiceExecutionOptions" ], function(openbis, CustomASServiceCode, CustomASServiceExecutionOptions) {
                // get a reference to AS API
                var v3 = new openbis();
                // login to obtain a session token (the token it is automatically stored in openbis object and will be used for all subsequent API calls)
                document.write("1.login");
                document.write("<br>");
                v3.login(USER, PASS).done(function(sessionToken) {
                    document.write("2.login - sessionToken: " + sessionToken);
                    document.write("<br>");

                    var id = new CustomASServiceCode("workshop-custom-as-api");
                    var options = new CustomASServiceExecutionOptions().withParameter("method", "echo").withParameter("message", "You are awesome!");
                    v3.executeCustomASService(id, options).done(function(result) {
                        document.write("3.executeCustomASService - result: " + result.result);
                        document.write("<br>");
                        // logout to release the resources related with the session
                        v3.logout();
                        document.write("4.logout");
                    });
                });
            });
        }
        executeCustomASService();
```

## Part 3 - How to extend openBIS ELN-LIMS using `ELN-LIMS Plugin Interface`

[ELN-LIMS Web UI Extensions](https://unlimited.ethz.ch/display/openBISDoc2010/ELN-LIMS+WEB+UI+extensions)

### ELN-LIMS Technology
* ELN-LIMS is the most used openBIS Technology
* Most people adopt openBIS as the solution for their lab due to the fact it includes this interface.
* Even if the interface has many functions these functions are mostly Generic.

### To solve business processes of your particular domain a plugin system was introduced with the idea of:
* Configure ELN-LIMS interface programatically.
* Add additional buttons to toolbars.
* Add additional components to Forms with special focus on Objects and DataSets.
* Add Extra Utilities to the Navigation Menu

### plugin.js Interface

[ELN-LIMS Plugin Interface](https://sissource.ethz.ch/sispub/openbis/-/blob/master/ui-eln-lims/src/core-plugins/eln-lims/1/as/webapps/eln-lims/html/js/config/ELNLIMSPlugin.js)

Main Functionalities of the plugin interface:
* Configuration methods
* UI Forms - Template methods
* UI Forms - Event methods
* UI Navigation - Extra Utility methods

The best detailed explanation with examples can be found at [ELN-LIMS Web UI Extensions]([ELN-LIMS Web UI Extensions](https://unlimited.ethz.ch/display/openBISDoc2010/ELN-LIMS+WEB+UI+extensions))

### Exercise 12 - Deploy and test the provided `workshop-eln-lims-plugin`

1. Deploy the plugin folder into `eln-lims/html/plugins`
2. Deploy the plugin into `etc/config.js`

```javascript
var PLUGINS_CONFIGURATION = {
    extraPlugins : ["life-sciences", "flow", "microscopy", "workshop-eln-lims-plugin"]
};
```

3. Open any Object form and a new button should appear calling the custom as service using an internal ELN method
