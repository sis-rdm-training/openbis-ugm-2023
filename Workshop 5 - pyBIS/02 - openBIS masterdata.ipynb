{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# How openBIS is organised"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The classic hierarchy of openBIS entities is a s follows:\n",
    "```\n",
    "space\n",
    "    project\n",
    "        experiment (collection)\n",
    "            sample (object)\n",
    "                dataset\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Space\n",
    "   * only members are allowed\n",
    "   * Roles typically are bound to a space, for example the role `space_admin` only applies to a specific space\n",
    "   * usually all members of a research group belong to a space"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Project\n",
    "   * is a container-like construct within a space\n",
    "   * It holds **eperiments** and **samples**\n",
    "   * can also be used for authorisation purposes, i.e. certain users within a space have access to a project, some don't\n",
    "   * contains no data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Experiment / Collection\n",
    "   * Collection is the new term for Experiment. Here, we will use the term Experiment\n",
    "   * lives within a project\n",
    "   * contains **samples** and **datasets**\n",
    "   * Everyone who can access a project can access all experiments in it, as well as all samples and datasets.\n",
    "   * can have **properties** (key-value pairs, metadata)\n",
    "   * properties are defined in the **experiment type**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Sample / Object\n",
    "   * probably the most often used entity in openBIS\n",
    "   * `object` is the new term for sample. Here we use the term sample, to not confuse it with a Python object\n",
    "   * holds a lot of **properties** (metadata) as defined in their **sample type**\n",
    "   * can have **parent/child relationship**, n:m relations are possible (many parents, many children)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dataset\n",
    "   * the central entity in openBIS\n",
    "   * contains actual **files and folders**\n",
    "   * has **properties** (metadata) as defined in their **dataset type**\n",
    "   * files are stored in the **openBIS Datastore**\n",
    "   * files can be **archived** for long-term storage (tape-drive) or **unarchived**\n",
    "   * dataset files are **immutable**, but properties can change\n",
    "   * can have **parent/child relationship**, n:m relations are possible (many parents, many children)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# openBIS masterdata\n",
    "\n",
    "The masterdata in openBIS stores the meta-meta information: \n",
    "\n",
    "```\n",
    "sample type, dataset type or experiment type\n",
    "    property type\n",
    "        data type\n",
    "            varchar\n",
    "            integer\n",
    "            real\n",
    "            XML\n",
    "            controlled vocabulary\n",
    "        vocabulary\n",
    "                \n",
    "vocabulary\n",
    "    vocabulary terms\n",
    "```\n",
    "\n",
    "The properties for experiments, samples and datasets are defined in their respective **entity type**:\n",
    "\n",
    "* experiment types\n",
    "* sample types\n",
    "* dataset types\n",
    "\n",
    "These entity types define which **properties** an entity has. The entity type is **mandatory for experiments, samples** and **datasets**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Authentication and Authorisation**\n",
    "\n",
    "OpenBIS also stores information about the **roles** that are assigned of every user and/or every group:\n",
    "\n",
    "```\n",
    "persons (users)\n",
    "    roles\n",
    "    \n",
    "groups\n",
    "    users\n",
    "    roles  \n",
    "```\n",
    "\n",
    "The roles are pre-defined:\n",
    "\n",
    "* INSTANCE_ADMIN\n",
    "* INSTANCE_OBSERVER\n",
    "* INSTANCE_ETL_SERVER\n",
    "* SPACE_ADMIN\n",
    "* SPACE_POWER_USER\n",
    "* SPACE_USER\n",
    "* SPACE_OBSERVER\n",
    "* SPACE_ETL_SERVER\n",
    "* PROJECT_ADMIN\n",
    "* PROJECT_POWER_USER\n",
    "* PROJECT_USER\n",
    "* PROJECT_OBSERVER\n",
    "\n",
    "Almost all authentication and authorisation can be organised via pyBIS."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Information about the openBIS server"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pybis import Openbis\n",
    "o = Openbis('localhost:8443', verify_certificates=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "o.hostname"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "o.get_server_information()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "o.get_datastores()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Property Types\n",
    "\n",
    "Properties are the fundamental way how to store metadata in openBIS. Every property is of a given `type` to ensure correct metadata is stored.\n",
    "For example, if we wanted to store the number of experiments, we would create a property type like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "o.get_property_types()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pt1 = o.new_property_type(\n",
    "    code        = 'NO_OF_EXP_RUNS', \n",
    "    label       = 'number of experiment runs', \n",
    "    description = 'number of experiment runs',\n",
    "    dataType    = 'INTEGER',\n",
    ")\n",
    "pt1.save()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We might also want to store the information whether the experiment was successful:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pt2 = o.new_property_type(\n",
    "    code        = 'EXP_SUCCESS', \n",
    "    label       = 'experiment was successful', \n",
    "    description = 'True if experiment was successful, False if not.',\n",
    "    dataType    = 'BOOLEAN',\n",
    ")\n",
    "pt2.save()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We also might want to see the current status of an experiment, using a controlled vocabulary."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "voc = o.new_vocabulary(\n",
    "    code = 'EXP_STATUS',\n",
    "    description = 'current status of the experiment',\n",
    "    terms = [\n",
    "        { \"code\": 'started', \"label\": \"started\", \"description\": \"Experiment started\"},\n",
    "        { \"code\": 'running', \"label\": \"running\", \"description\": \"Experiment is running\"},\n",
    "        { \"code\": 'finished', \"label\": \"finished\", \"description\": \"Experiment finished\"}\n",
    "    ]\n",
    ")\n",
    "voc.save()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For our third property, we want to make sure only `started`, `running` or `finished` are allowed strings."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pt3 = o.new_property_type(\n",
    "    code        = 'EXP_STATUS', \n",
    "    label       = 'experiment status', \n",
    "    description = 'current status of the experiment',\n",
    "    dataType    = 'CONTROLLEDVOCABULARY',\n",
    "    vocabulary  = voc.code\n",
    ")\n",
    "pt3.save()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally we define an experiment type which contains these properties:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "expt = o.new_experiment_type(code=\"MY_EXCITING_EXPERIMENT\", description=\"my exciting new experiment\")\n",
    "expt.save()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once the experiment is successfully saved, we assign the property types above to make them available as properties to every experiment we create:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "expt.assign_property(pt1)\n",
    "expt.assign_property(pt2)\n",
    "expt.assign_property(pt3)\n",
    "\n",
    "# also possible:\n",
    "# expt.assign_property('NO_OF_EXP_RUNS')\n",
    "# expt.assign_property(pt2.code)\n",
    "# expt.assign_property(prop=pt3, ordinal=1, mandatory=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we should check whether all property assignments have been properly stored:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "expt.get_property_assignments()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In similar fashion it is possilbe to remove property assigments with the usage of `revoke_property` method"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "expt.revoke_property(pt3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "expt.get_property_assignments()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Sample types\n",
    "\n",
    "Get the list of all available sample types."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sample_types = o.get_sample_types()\n",
    "sample_types"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Get a single item of the list above by either referring to its array index:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "st = sample_types[12]\n",
    "st"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "... or by looking it up by its `code`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "st = o.get_sample_type('EXPERIMENTAL_STEP')\n",
    "st"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Properties in a sample_type\n",
    "\n",
    "Every sample type (or dataset type, experiment type) defines a number of attributes for every sample. These properties are *assigned* to a given sample type, hence `get_property_assignments()` returns the list:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "st.get_property_assignments()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Properties\n",
    "\n",
    "Every property (or property type) is of a given `dataType` (VARCHAR, MULTILINE_VARCHAR, INTEGER, XML, CONTROLLEDVOCABULARY, etc.), has a `label`, a `description`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "o.get_property_type('PUBLICATION')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Dataset Types\n",
    "\n",
    "Dataset types behave the same way as sample types, in short:\n",
    "\n",
    "- a dataset type contains a number of properties\n",
    "- every property is of a given dataType\n",
    "- a property can be assigned to a vocabulary, in case of dataType=CONTROLLEDVOCABULARY"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "dst = o.get_dataset_types()\n",
    "dst"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# get the 5. item in the list above\n",
    "o.get_dataset_types()[6]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# fetch a dataset type by its code\n",
    "dt = o.get_dataset_type('RAW_DATA')\n",
    "dt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Experiment types"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Experiments, like Samples and Datasets, need to be of a specific **experiment type**, which acts similarly."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "o.get_experiment_types()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Vocabulary and Terms"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Some of the **property types** are of dataType `CONTROLLED VOCABULARY`. A controlled vocabulary means: you cannot enter any string you want. Therefore, a **vocabulary** consists of a number of **terms**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "o.get_vocabularies()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To list the terms of a vocabulary, just use the `.get_terms()` method on a specific vocabulary:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "o.get_vocabulary('$STORAGE_FORMAT').get_terms()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Plugins\n",
    "\n",
    "Plugins are Jython scripts which live on the server. They are able to do **additional data checks** when a sample or dataset is being registered or updated. A plugin can be assigned to a  sample type, dataset type or experiment type. Whenever a sample/dataset/experiment of that type is registered, the plugin will be executed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "o.get_plugins()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# ELN-LIMS settings"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To alter the ELN-LIMS settings, you first need to: \n",
    "\n",
    "* visit the ELN-LIMS browser\n",
    "* switch to Utilities -> Settings\n",
    "* click on the Edit button and then again on Save\n",
    "\n",
    "This will store the default settings to a sample which we then can modify via pyBIS."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 36,
   "metadata": {},
   "outputs": [],
   "source": [
    "import json\n",
    "settings_sample = o.get_sample(\"/ELN_SETTINGS/GENERAL_ELN_SETTINGS\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The settings are stored in JSON format, so we first need to convert them into a Python dictionary:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "settings = json.loads(settings_sample.props[\"$eln_settings\"])\n",
    "settings[\"mainMenu\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Main Menu settings\n",
    "To modify the Main Menu settings, you have to:\n",
    "\n",
    "* change the settings dictionary\n",
    "* convert the settings back to json format\n",
    "* save the sample"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "settings['mainMenu']['showTrashcan'] = False\n",
    "settings['mainMenu']['showBarcodes'] = True\n",
    "settings_sample.props['$eln_settings'] = json.dumps(settings)\n",
    "settings_sample.save()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## ELN Storage settings\n",
    "The ELN storages settings can be found in the samples of project `/ELN_SETTINGS/STORAGES`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "o.get_samples(project='/ELN_SETTINGS/STORAGES')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For example, to change the settings of `BENCH`, just change the sample's properties and save the sample:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sto = o.get_sample('/ELN_SETTINGS/STORAGES/BENCH')\n",
    "sto.props()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sto.props['$storage.box_space_warning']= 50\n",
    "sto.save()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## ELN Templates\n",
    "\n",
    "The ELN templates settings can be found in the samples of project `/ELN_SETTINGS/TEMPLATES`. Updating works similar to ELN Storage settings."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "o.get_samples(project='/ELN_SETTINGS/TEMPLATES')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Custom Widgets\n",
    "To change the Custom Widgets settings, get the `property_type` and set the metaData attribute:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pt = o.get_property_type('YEAST.SOURCE')\n",
    "pt.metaData = {'custom_widget': 'Spreadsheet'}\n",
    "pt.save()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Currently, the value of the `custom_widget` key can be set to either\n",
    "\n",
    "* `Spreadsheet` (for tabular, Excel-like data)\n",
    "* `Word Processor` (for rich text data)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
