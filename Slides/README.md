This directory contains the slides of the presentations given during the furst day of the openBIS UGM, 12.09.2023.

The recordings of the presentations are avilable here: https://video.ethz.ch/events/2023/openbis.html 


1. **From Pilot to Rollout: Introducing the BAM Data Store for Institutional RDM in Materials Science**.  
Dr. Rukeia El-Athman, Bundesanstalt für Materialforschung und –prüfung (BAM)

2. **Introducing openBIS @ Empa**.  
Bachofner Anusch, Empa

3. **openBIS in an experimental lab environment**.  
Hauser Stefanie, Empa

4. **openBIS-Che​f: cooking easy UIs for you**.  
Roesslein Matthias; Baffelli Simone, Empa

5. **openBIS @ Fraunhofer IWM: Creating an Infrastructure for Digitization**.  
Hafok Heiko, Fraunhofer IWM

6. **Large-File Raw Data Synchronization for openBIS Research Repositories**.  
Ringwald Friedemann, Universitäts Klinikum Heidelberg

7. **openBIS and pyBIS in a data management pipeline**.  
You Guanghao, NCCR Evolving Language

8. **RDM in Heterogeneous Collaborations**.  
Kerzel Ulrich, RWTH Aachen University

9. **openBIS: latest developments & roadmap**.  
Barillari Caterina, ETH Zurich
